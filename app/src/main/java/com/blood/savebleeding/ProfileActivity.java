package com.blood.savebleeding;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;

import de.hdodenhof.circleimageview.CircleImageView;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.threeten.bp.LocalDate;
import org.threeten.bp.Period;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.temporal.ChronoUnit;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

public class ProfileActivity extends AppCompatActivity {

    TextView fullname, fname, lname, companyname, email, mobile, country, city, gender, bloodgroup,
            age, address;

    CircleImageView profileimg;
    ProgressDialog progressDialog;
    ToggleButton donationStatus, accountStatus;

    LinearLayout donationBox;
    LinearLayout box1, box2, box3, genderBox, ageBox , statusBox , addressbox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        donationStatus = findViewById(R.id.donationToggleButton);
        accountStatus = findViewById(R.id.accountToggleButton);

        profileimg = findViewById(R.id.image);
        fullname = findViewById(R.id.fullname);
        fname = findViewById(R.id.fname);
        lname = findViewById(R.id.lname);
        companyname = findViewById(R.id.companyname);
        email = findViewById(R.id.email);
        mobile = findViewById(R.id.mobile);
        country = findViewById(R.id.country);
        city = findViewById(R.id.cityProfile);
        gender = findViewById(R.id.gender);
        bloodgroup = findViewById(R.id.bloodgroup);
        age = findViewById(R.id.age);
        address = findViewById(R.id.addressProfileActivity);
        ageBox = findViewById(R.id.ageBox);
        genderBox = findViewById(R.id.genderBox);
        addressbox = findViewById(R.id.addressBox);

        box1 = findViewById(R.id.box1);
        box2 = findViewById(R.id.box2);
        box3 = findViewById(R.id.box3);
        donationBox = findViewById(R.id.donationBox);
        statusBox = findViewById(R.id.accountStatusBox);

        if (MyClass.user.getType().equals("individual")) {
            checkUserAge();
        }
        updateProfileImage();
    }

    private void checkUserAge() {
        SharedPreferences sp1 = getSharedPreferences("LoginCredientials", 0);
        String userDob = MyClass.user.age;
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate userDobFormatted = LocalDate.parse(userDob, dateTimeFormatter);
        int userAge = Period.between(userDobFormatted, LocalDate.now()).getYears();
        String userGender = MyClass.user.gender;
        if (userGender.toLowerCase().equals("male")) {
            if (userAge >= 18 && userAge < 50) {
                donationBox.setVisibility(View.INVISIBLE);
            } else if (userAge < 18) {
                donationBox.setVisibility(View.INVISIBLE);
            } else {
                donationBox.setVisibility(View.VISIBLE);
            }
        } else {
            if (userAge < 18) {
                donationBox.setVisibility(View.INVISIBLE);
            } else {
                donationBox.setVisibility(View.VISIBLE);
            }
        }
//        if (userAge < 18) {
//            Toast.makeText(this, "here", Toast.LENGTH_SHORT).show();
//            donationBox.setVisibility(View.GONE);
//
//        } else {
//            if (userGender.toLowerCase().equals("male") && userAge > 50) {
//                donationBox.setVisibility(View.VISIBLE);
//                checkUserHistoryData();
//            } else if (userGender.toLowerCase().equals("male") && userAge <= 50) {
//                donationBox.setVisibility(View.GONE);
//            } else if (userGender.toLowerCase().equals("male") && userAge == 18) {
//                donationBox.setVisibility(View.VISIBLE);
//
//            } else if (userGender.toLowerCase().equals("female")) {
//                donationBox.setVisibility(View.VISIBLE);
//                checkUserHistoryData();
//            }
//        }


    }

    private void checkUserHistoryData() {
        SharedPreferences sp1 = getSharedPreferences("LoginCredientials", 0);
        String phone = sp1.getString("phone", "none");
        //if user has some existing data
        FirebaseDatabase.getInstance().getReference().child("history").
                child(phone).limitToLast(1).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot dsp : dataSnapshot.getChildren()) {
                        String Date = dsp.getValue(String.class);
                        LocalDate currentDate = LocalDate.now();
                        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
                        assert Date != null;
                        LocalDate donationDate = LocalDate.parse(Date, dateTimeFormatter);
                        long daysDuaration = ChronoUnit.DAYS.between(donationDate, currentDate);
                        if (daysDuaration <= 90) {
                            donationStatus.setChecked(false);
                            donationStatus.setEnabled(false);
                            Toast.makeText(ProfileActivity.this, "You can't turn on Donation Status because the 90 day limit is not over yet", Toast.LENGTH_SHORT).show();

                        } else {
                            donationStatus.setEnabled(true);
                        }

                    }
                } else {
                    SharedPreferences sp1 = getSharedPreferences("LoginCredientials", 0);
                    String phone = sp1.getString("phone", "none");

                    FirebaseDatabase.getInstance().getReference().child("users_updated").child(phone)
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                    MyClass.user = snapshot.getValue(User.class);
                                    Log.d("heh", "onDataChange: " + MyClass.user.getDonationStatus());
                                    if (MyClass.user.getDonationStatus()) {
                                        donationStatus.setChecked(true);
                                    } else {
                                        donationStatus.setChecked(false);
                                    }

                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {

                                }
                            });

                }
            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void onAccountStatusClicked(View view) {

        SharedPreferences sp1 = getSharedPreferences("LoginCredientials", 0);
        String phone = sp1.getString("phone", "none");
        if (accountStatus.isChecked()) {
            assert phone != null;
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("users_updated").child(phone);
            Map<String, Object> updates = new HashMap<String, Object>();
            updates.put("accountStatus", "disabled");
            ref.updateChildren(updates);
        } else {
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("users_updated").child(phone);
            Map<String, Object> updates = new HashMap<String, Object>();
            updates.put("accountStatus", "enabled");
            ref.updateChildren(updates);
        }

    }

    public void onDonationStatusClicked(View view) {
        SharedPreferences sp1 = getSharedPreferences("LoginCredientials", 0);
        String phone = sp1.getString("phone", "none");
        if (donationStatus.isChecked()) {
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("users_updated").child(phone);
            Map<String, Object> updates = new HashMap<String, Object>();
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            updates.put("phonePreference", true);
                            updates.put("donationStatus", true);
                            ref.updateChildren(updates);
                            donationStatus.setChecked(true);
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            updates.put("donationStatus", false);
                            updates.put("phonePreference", false);
                            ref.updateChildren(updates);
                            donationStatus.setChecked(false);

                            break;
                    }
                }
            };
            MyClass.ShowDonationStatusDialogBox(this, dialogClickListener, "Attention", "If you will turn Donation Status " +
                    "on then your phone number will be shown when people will search for users." +
                    "Do yo want to continue");

        } else {
            assert phone != null;
            MyClass.showError(this, "Attention", "If Donation Status is " +
                    "OFF then your phone number will not be shown when people will search for users.");
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("users_updated").child(phone);
            Map<String, Object> updates = new HashMap<String, Object>();
            updates.put("donationStatus", false);
            updates.put("phonePreference", false);
            ref.updateChildren(updates);
            donationStatus.setChecked(false);

        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (MyClass.user.getType().equals("individual")) {
            fullname.setText(MyClass.user.getFullname());
            fname.setText(MyClass.user.getFirstname());
            lname.setText(MyClass.user.getLastname());
            email.setText(MyClass.user.getEmail());
            mobile.setText(MyClass.user.getMobile());
            country.setText(MyClass.user.getCountry());
            city.setText(MyClass.user.getCity());
            gender.setText(MyClass.user.getGender());
            address.setText(MyClass.user.getAddress());
            box1.setVisibility(View.GONE);
            box2.setVisibility(View.VISIBLE);
            box3.setVisibility(View.VISIBLE);
        } else {
            Log.d("iuser", "onDataChange: " + MyClass.user.getEmail());
            fullname.setText(MyClass.user.getCompanyname());
            companyname.setText(MyClass.user.getCompanyname());
            box1.setVisibility(View.VISIBLE);
            box2.setVisibility(View.GONE);
            box3.setVisibility(View.GONE);
            donationBox.setVisibility(View.GONE);
            donationStatus.setVisibility(View.GONE);
            genderBox.setVisibility(View.GONE);
            ageBox.setVisibility(View.GONE);
            addressbox.setVisibility(View.GONE);
            email.setText(MyClass.user.getCompanyEmailAdress());
            mobile.setText(MyClass.user.getMobile());
            country.setText(MyClass.user.getCountry());
            city.setText(MyClass.user.getCity());
            bloodgroup.setText(MyClass.user.getBloodgroup());
        }
        if (MyClass.user.getType().equals("individual")) {
            checkUserAge();
            email.setText(MyClass.user.getEmail());
            mobile.setText(MyClass.user.getMobile());
            country.setText(MyClass.user.getCountry());
            city.setText(MyClass.user.getCity());
            gender.setText(MyClass.user.getGender());
            address.setText(MyClass.user.getAddress());
            String userDob = MyClass.user.age;
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            LocalDate userDobFormatted = LocalDate.parse(userDob, dateTimeFormatter);
            int userAge = Period.between(userDobFormatted, LocalDate.now()).getYears();
            age.setText(String.valueOf(userAge));
            bloodgroup.setText(MyClass.user.getBloodgroup());
            if (donationBox.getVisibility() == View.VISIBLE) {
                checkUserHistoryData();
            } else {
                checkUserAge();
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onEdit(View view) {
        startActivity(new Intent(this, EditProfileActivity.class));
    }

    public void onImage(View view) {
        RxPermissions rxPermissions = new RxPermissions(this);

        rxPermissions.request(Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA).subscribe(granted -> {

            if (granted) {
                PopupMenu popup = new PopupMenu(ProfileActivity.this, view);
                popup.getMenuInflater().inflate(R.menu.profile_image_menu, popup.getMenu());

                popup.setOnMenuItemClickListener(item -> {

                    switch (item.getItemId()) {
                        case R.id.action_change:
                            changeProfileImage();
                            break;

                        case R.id.action_remove:
                            removeProfileImage();
                            break;
                    }

                    return true;
                });

                popup.show();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            Uri filePath = result.getUri();
            uploadImage(filePath);
        }
    }

    void uploadImage(Uri filePath) {
        if (filePath != null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Updating profile image...");
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();

            StorageReference ref = FirebaseStorage.getInstance().getReference().child("images")
                    .child(UUID.randomUUID().toString());

            ref.putFile(filePath)
                    .addOnCompleteListener(task -> {

                        if (task.isSuccessful()) {
                            ref.getDownloadUrl().addOnSuccessListener(uri -> {

                                FirebaseDatabase.getInstance().getReference().child("users_updated")
                                        .child(MyClass.user.getMobile()).child("image")
                                        .setValue(uri.toString())
                                        .addOnCompleteListener(task1 -> {

                                            if (task1.isSuccessful()) {
                                                MyClass.user.setImage(uri.toString());
                                                updateProfileImage();
                                                progressDialog.dismiss();
                                            }
                                        });
                            });
                        } else {
                            progressDialog.dismiss();
                            MyClass.showError(ProfileActivity.this, "Submit Failed!", task.getException().getMessage());
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                            progressDialog.setMessage("Updating " + (int) progress + "%");
                        }
                    });
        }
    }

    void changeProfileImage() {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setFixAspectRatio(true)
                .start(this);
    }

    void updateProfileImage() {
        if (MyClass.user.getImage() != null && !MyClass.user.getImage().equals("none")) {
            Picasso.get()
                    .load(MyClass.user.getImage())
                    .placeholder(R.drawable.image_default)
                    .into(profileimg);
        } else
            profileimg.setImageResource(R.drawable.image_default);
    }

    void removeProfileImage() {
        if (MyClass.user.getImage() != null && !MyClass.user.getImage().equals("none")) {
            StorageReference storageRef = FirebaseStorage.getInstance().getReferenceFromUrl(MyClass.user.getImage());
            storageRef.delete().addOnCompleteListener(task -> {

                if (task.isSuccessful()) {
                    FirebaseDatabase.getInstance().getReference().child("users_updated").child(MyClass.user.getMobile()).child("image")
                            .setValue("none")
                            .addOnCompleteListener(task1 -> {

                                if (task1.isSuccessful()) {
                                    MyClass.user.setImage("none");
                                    updateProfileImage();
                                }
                            });
                }
            });
        }
    }

}