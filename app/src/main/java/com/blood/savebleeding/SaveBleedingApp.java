package com.blood.savebleeding;

import android.app.Application;

import com.jakewharton.threetenabp.AndroidThreeTen;

public class SaveBleedingApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        AndroidThreeTen.init(this);
    }
}
