package com.blood.savebleeding;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.threeten.bp.LocalDate;
import org.threeten.bp.Period;
import org.threeten.bp.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;

public class FindDonorsByCnicActivity extends AppCompatActivity {
    private SearchView searchView;
    TextView fname, lname, email, mobile, country, city, gender, bloodgroup, age, donationStatus , address;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_donors_by_cnic);
        searchView = findViewById(R.id.searchView);

        fname = findViewById(R.id.firstname);
        lname = findViewById(R.id.lastname);
        email = findViewById(R.id.email);
        mobile = findViewById(R.id.mobile);
        country = findViewById(R.id.country);
        city = findViewById(R.id.cityDonor);
        gender = findViewById(R.id.gender);
        address = findViewById(R.id.addressCnicActivity);
        bloodgroup = findViewById(R.id.bloodgroup);
        age = findViewById(R.id.ageCnicActivity);
        donationStatus = findViewById(R.id.donationStatus);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                String cninNumberWithoutSpaces = query.replaceAll("\\s+", "");
                int cnicLength = cninNumberWithoutSpaces.length();
                if (cnicLength == 13) {
                    final ProgressDialog progressDialog = new ProgressDialog(FindDonorsByCnicActivity.this);
                    progressDialog.setMessage("Finding match, please wait...");
                    progressDialog.setCancelable(false);
                    progressDialog.setCanceledOnTouchOutside(false);
                    progressDialog.show();
                    FirebaseDatabase.getInstance().getReference().child("users_updated")
                            .orderByChild("cnic").equalTo(query.trim())
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.exists()) {
                                        Log.d("gg", "onDataChange: " + dataSnapshot.getChildrenCount());
                                        for (DataSnapshot dsp : dataSnapshot.getChildren()) {
                                            User user = dsp.getValue(User.class);
                                            assert user != null;
                                            String userDob = user.getAge();
                                            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
                                            LocalDate userDobFormatted = LocalDate.parse(userDob, dateTimeFormatter);
                                            int userAge = Period.between(userDobFormatted, LocalDate.now()).getYears();
                                            if (MyClass.user.getCnic().equals(user.getCnic()))
                                            {
                                                MyClass.showError(FindDonorsByCnicActivity.this,"Sorry","You can not search your own account");
                                                progressDialog.dismiss();
                                                return;
                                            }
                                            if (user.getAccountStatus().equals("disabled")) {
                                                MyClass.showError(FindDonorsByCnicActivity.this, "Sorry", "The User has disabled his account");
                                                progressDialog.dismiss();
                                            } else {
                                                if (user.getGender().toLowerCase().equals("female")) {
                                                    if (user.getPhonePreference()) {
                                                        mobile.setText(Html.fromHtml("<u>" + dsp.getKey() + "</u>"));
                                                        donationStatus.setText("Is Willing to Donate");

                                                    } else {
                                                        mobile.setText("This user Choose not to show her phone number.");
                                                        donationStatus.setText("Not Willing to Donate");
                                                    }
                                                } else {
                                                    if (userAge < 50) {
                                                        if (user.getPhonePreference()) {
                                                            mobile.setText(Html.fromHtml("<u>" + dsp.getKey() + "</u>"));
                                                            donationStatus.setText("Not Willing to Donate");

                                                        } else {
                                                            mobile.setText("This user Choose not to show his phone number.");
                                                            donationStatus.setText("Is Willing to Donate");

                                                        }
                                                    } else {
                                                        if (user.getPhonePreference()) {
                                                            mobile.setText(Html.fromHtml("<u>" + dsp.getKey() + "</u>"));
                                                            donationStatus.setText("Is Willing to Donate");

                                                        } else {
                                                            mobile.setText("This user Choose not to show his phone number.");
                                                            donationStatus.setText("Not Willing to Donate");


                                                        }
                                                    }
                                                }
                                                fname.setText(user.getFirstname());
                                                lname.setText(user.getLastname());
                                                email.setText(user.getEmail());
                                                country.setText(user.getCountry());
                                                city.setText(user.getCity());
                                                gender.setText(user.getGender());
                                                bloodgroup.setText(user.getBloodgroup());
                                                age.setText(String.valueOf(userAge));
                                                address.setText(user.getAddress());
                                                progressDialog.dismiss();
                                            }
                                        }

                                    } else {
                                        progressDialog.dismiss();
                                        MyClass.showError(FindDonorsByCnicActivity.this, "Record Error!", "No Record Found.");
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                    progressDialog.dismiss();
                                    MyClass.showError(FindDonorsByCnicActivity.this, "Database Error!", databaseError.getMessage());
                                }
                            });
                } else {
                    MyClass.showError(FindDonorsByCnicActivity.this, "Input Error", "CNIC Number Not Valid Please Enter a Correct CNIC Number");
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        mobile.setOnClickListener(view -> {
            onCall();

        });

    }

    private void onCall() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.CALL_PHONE},
                    MyClass.RC_CALL);
        } else {
            startActivity(new Intent(Intent.ACTION_CALL).setData(Uri.parse("tel:" + mobile.getText())));
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {

            case MyClass.RC_CALL:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    onCall();
                } else {
                    Log.d("TAG", "Call Permission Not Granted");
                }
                break;

            default:
                break;
        }
    }
}