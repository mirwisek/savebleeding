package com.blood.savebleeding;

public class User {
    String bloodgroup;
    String city;
    String country;
    String email;
    String firstname;
    String lastname;
    String companyname;
    String gender;
    String mobile;
    String image;
    String type;
    String cnic;
    String age;
    Boolean showPhoneNumber;
    Boolean donationStatus;
    String accountStatus;
    Boolean phonePreference;
    String address;
    String companyEmailAdress;


    public String getBloodgroup() {
        return bloodgroup;
    }

    public void setBloodgroup(String bloodgroup) {
        this.bloodgroup = bloodgroup;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFullname() {
        return firstname + " " + lastname;
    }

    public String getCnic() {
        return cnic;
    }

    public void setCnic(String cnic) {
        this.cnic = cnic;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public Boolean getShowPhoneNumber() {
        return showPhoneNumber;
    }

    public void setShowPhoneNumber(Boolean showPhoneNumber) {
        this.showPhoneNumber = showPhoneNumber;
    }

    public Boolean getDonationStatus() {
        return donationStatus;
    }

    public void setDonationStatus(Boolean donationStatus) {
        this.donationStatus = donationStatus;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public Boolean getPhonePreference() {
        return phonePreference;
    }

    public void setPhonePreference(Boolean phonePreference) {
        this.phonePreference = phonePreference;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCompanyEmailAdress() {
        return companyEmailAdress;
    }

    public void setCompanyEmailAdress(String companyEmailAdress) {
        this.companyEmailAdress = companyEmailAdress;
    }
}
