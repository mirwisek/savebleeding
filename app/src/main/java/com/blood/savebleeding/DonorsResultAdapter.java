package com.blood.savebleeding;

import android.Manifest;
import android.app.Activity;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import de.hdodenhof.circleimageview.CircleImageView;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.threeten.bp.LocalDate;
import org.threeten.bp.Period;
import org.threeten.bp.format.DateTimeFormatter;

import java.util.List;

public class DonorsResultAdapter extends RecyclerView.Adapter<DonorsResultAdapter.MyViewHolder> {

    private List<User> userList;
    private Context context;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView name, mobile, gender, donationStatus;
        CircleImageView image;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            mobile = view.findViewById(R.id.mobile);
            gender = view.findViewById(R.id.gender);
            image = view.findViewById(R.id.image);
            donationStatus = view.findViewById(R.id.donationStatus);
        }
    }

    public DonorsResultAdapter(Context context, List<User> userList) {
        this.userList = userList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyc_donor_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        final User user = userList.get(position);
//        if (MyClass.user.getMobile().equals(user.getMobile())) {
//            holder.itemView.setVisibility(View.GONE);
//            holder.itemView.setLayoutParams(new RecyclerView.LayoutParams(0, 0));
//        } else {
//            holder.itemView.setVisibility(View.VISIBLE);
//            holder.itemView.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//        }
//
//        if (user.getAccountStatus().toLowerCase().equals("disabled")) {
//            holder.itemView.setVisibility(View.GONE);
//            holder.itemView.setLayoutParams(new RecyclerView.LayoutParams(0, 0));
//        } else {
//            holder.itemView.setVisibility(View.VISIBLE);
//            holder.itemView.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//        }
//        if (MyClass.getAgeByDob(user.getAge()) < 18) {
//
//            holder.itemView.setVisibility(View.GONE);
//            holder.itemView.setLayoutParams(new RecyclerView.LayoutParams(0, 0));
//        } else {
//            holder.itemView.setVisibility(View.VISIBLE);
//            holder.itemView.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//        }

        if (user.getDonationStatus()) {
            holder.donationStatus.setText("Is Willing To Donate Blood");
            holder.mobile.setText(user.getMobile());
            holder.donationStatus.setTextColor(context.getResources().getColor(R.color.Green));
        } else {
            holder.donationStatus.setText("Not Willing to Donate Blood");
            holder.mobile.setText("Not Available");
            holder.donationStatus.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        }
//        if (user.getGender().toLowerCase().equals("female")) {
//            if (user.getDonationStatus()) {
//                holder.donationStatus.setText("Is Willing To Donate Blood");
//                holder.donationStatus.setTextColor(context.getResources().getColor(R.color.colorPrimary));
//
//            } else {
//                holder.donationStatus.setText("Not Willing to Donate Blood");
//                holder.mobile.setText("Not Available");
//
//            }
//        } else {
//            if (Integer.parseInt(user.getAge()) < 50) {
//                holder.donationStatus.setText("Not Willing to Donate Blood");
//                holder.mobile.setText("Not Available");
//            } else {
//                if (user.getDonationStatus()) {
//                    holder.donationStatus.setText("Is Willing To Donate Blood");
//                } else {
//                    holder.donationStatus.setText("Not Willing to Donate Blood");
//                    holder.mobile.setText("Not Available");
//
//                }
//            }
//        }


        holder.name.setText(user.getFullname());
        holder.gender.setText(user.getGender());

        Picasso.get()
                .load(user.getImage())
                .placeholder(R.drawable.image_default)
                .into(holder.image);

        holder.mobile.setOnClickListener(view -> {
            int permissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE);
            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(
                        (Activity) context,
                        new String[]{Manifest.permission.CALL_PHONE},
                        MyClass.RC_CALL);
            }
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" + user.getMobile()));
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }


}
