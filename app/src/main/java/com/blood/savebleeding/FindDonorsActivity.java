package com.blood.savebleeding;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

import com.ybs.countrypicker.CountryPicker;
import com.ybs.countrypicker.CountryPickerListener;

public class FindDonorsActivity extends AppCompatActivity {

    TextView country;
    Spinner city, bloodgroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_donors);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        country = findViewById(R.id.country);
        city = findViewById(R.id.city);
        bloodgroup = findViewById(R.id.bloodgroup);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

//    public void onCountry(View view)
//    {
//        final CountryPicker picker = CountryPicker.newInstance("Select Country");  // dialog title
//        picker.setListener(new CountryPickerListener() {
//            @Override
//            public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {
//                // Implement your code here
//
//                country.setText(name);
//                picker.dismiss();
//            }
//        });
//        picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
//    }

    public void onFind(View view)
    {
        if (city.getSelectedItem().toString().equals("Select City") ||
                bloodgroup.getSelectedItem().toString().equals("Select Blood Group") )
        {
            MyClass.showError(this, "Searching Failed!", "Please select all required fields.");
            return;
        }

        Intent intent = new Intent(this, DonorsResultActivity.class);
        intent.putExtra("country", "Pakistan");
        intent.putExtra("city", city.getSelectedItem().toString());
        intent.putExtra("bloodgroup", bloodgroup.getSelectedItem().toString());
        startActivity(intent);
    }
}
