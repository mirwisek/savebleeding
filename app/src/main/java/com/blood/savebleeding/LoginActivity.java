package com.blood.savebleeding;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.santalu.maskedittext.MaskEditText;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class LoginActivity extends AppCompatActivity {

    EditText password;
    MaskEditText mobile;
    FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        password = findViewById(R.id.password);
        mobile = findViewById(R.id.mobile);
        auth = FirebaseAuth.getInstance();
    }

    public void onLogin(View view) {

        if (TextUtils.isEmpty(mobile.getText().toString().trim()) || TextUtils.isEmpty(password.getText().toString().trim())) {
            MyClass.showError(this, "Login Failed!", "Please enter all required fields.");
            return;
        }
        String mobileNumber = mobile.getText().toString();
        if (mobileNumber.startsWith("0"))
        {
            String mobileNumberWithoutCountryCode = mobile.getText().toString().substring(1);
            mobileNumber = "+92"+ mobileNumberWithoutCountryCode ;
        }
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("logging in, please wait...");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        FirebaseDatabase.getInstance().getReference().child("users_updated").child(mobileNumber)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        if (dataSnapshot.exists()) {
                            progressDialog.dismiss();
                            String userEmail = dataSnapshot.child("email").getValue(String.class);
                            assert userEmail != null;
                            auth.signInWithEmailAndPassword(userEmail, password.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        MyClass.user = dataSnapshot.getValue(User.class);
                                        assert MyClass.user != null;
                                        MyClass.user.setMobile(dataSnapshot.getKey());
                                        SharedPreferences sp = getSharedPreferences("LoginCredientials", 0);
                                        SharedPreferences.Editor Ed = sp.edit();
                                        Ed.putString("phone", dataSnapshot.getKey());
                                        Ed.apply();
                                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("users_updated")
                                                .child(Objects.requireNonNull(dataSnapshot.getKey()));
                                        Map<String, Object> updates = new HashMap<String, Object>();
                                        updates.put("password", password.getText().toString());
                                        ref.updateChildren(updates);
                                        Intent intent = new Intent(LoginActivity.this, ReceptorActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                    } else {
                                        MyClass.showError(LoginActivity.this, "Login Failed!", Objects.requireNonNull(task.getException()).toString());
                                    }
                                }
                            });

//                            if (dataSnapshot.child("password").getValue().equals(password.getText().toString()))
//                            {
//                                MyClass.user = dataSnapshot.getValue(User.class);
//                                MyClass.user.setMobile(dataSnapshot.getKey());
//
//                                SharedPreferences sp = getSharedPreferences("LoginCredientials", 0);
//                                SharedPreferences.Editor Ed = sp.edit();
//                                Ed.putString("phone", dataSnapshot.getKey());
//                                Ed.commit();
//
//                                Intent intent = new Intent(LoginActivity.this, ReceptorActivity.class);
//                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
//                                startActivity(intent);
//                                finish();
//                            }
//                            else
//                            {
//                                MyClass.showError(LoginActivity.this, "Login Failed!", "Invalid account password.");
//                            }
                        } else {
                            progressDialog.dismiss();
                            MyClass.showError(LoginActivity.this, "Login Failed!", "Mobile number not found.");
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    public void onCreateNewAccount(View view) {
        startActivity(new Intent(this, SignUpActivity.class));
    }

    public void onForgotPassword(View view) {
        startActivity(new Intent(this, ForgotPasswordActivity.class));
    }

}
