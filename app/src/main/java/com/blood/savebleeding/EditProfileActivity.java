package com.blood.savebleeding;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class EditProfileActivity extends AppCompatActivity {

    EditText fname, lname, companyname, email;

    LinearLayout box1, box2, box3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        fname = findViewById(R.id.fname);
        lname = findViewById(R.id.lname);
        companyname = findViewById(R.id.companyname);
        email = findViewById(R.id.email);

        box1 = findViewById(R.id.box1);
        box2 = findViewById(R.id.box2);
        box3 = findViewById(R.id.box3);

        email.setText( MyClass.user.getEmail() );

        if (MyClass.user.getType().equals("individual"))
        {
            fname.setText( MyClass.user.getFirstname() );
            lname.setText( MyClass.user.getLastname() );
            box1.setVisibility(View.GONE);
            box2.setVisibility(View.VISIBLE);
            box3.setVisibility(View.VISIBLE);
        }
        else
        {
            companyname.setText(MyClass.user.getCompanyname());
            box1.setVisibility(View.VISIBLE);
            box2.setVisibility(View.GONE);
            box3.setVisibility(View.GONE);
        }
    }

    public void onUpdate(View view)
    {
        HashMap<String, Object> map = new HashMap<>();

        if (MyClass.user.equals("individual"))
        {
            map.put("firstname", fname.getText().toString());
            map.put("lastname", lname.getText().toString());
        }
        else
            map.put("companyname", companyname.getText().toString() );

        map.put("email", email.getText().toString());

        FirebaseDatabase.getInstance().getReference().child("users_updated").child(MyClass.user.getMobile())
                .updateChildren(map)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        
                        if (task.isSuccessful())
                        {
                            MyClass.user.setFirstname( fname.getText().toString() );
                            MyClass.user.setLastname( lname.getText().toString() );
                            MyClass.user.setCompanyname( companyname.getText().toString() );
                            MyClass.user.setEmail( email.getText().toString() );

                            Toast.makeText(EditProfileActivity.this, "Profile Updated Successfully.", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                });
    }
}
