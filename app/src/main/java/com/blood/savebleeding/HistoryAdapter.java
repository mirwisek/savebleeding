package com.blood.savebleeding;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.MyViewHolder> {

    private List<String> statusList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView status;

        public MyViewHolder(View view) {
            super(view);
            status = view.findViewById(R.id.status);
        }
    }

    public HistoryAdapter(Context context, List<String> statusList) {
        this.statusList = statusList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyc_history_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final String status = statusList.get(position);

        holder.status.setText(status);
    }

    @Override
    public int getItemCount() {
        return statusList.size();
    }
}
