package com.blood.savebleeding;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hbb20.CountryCodePicker;
import com.santalu.maskedittext.MaskEditText;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;


import org.threeten.bp.LocalDate;
import org.threeten.bp.Period;
import org.threeten.bp.format.DateTimeFormatter;

import java.util.Calendar;
import java.util.HashMap;

import java.util.Objects;


public class SignUpActivity extends AppCompatActivity {

    EditText fname, lname, companyname, email, password, age, address;
    TextView genderText;
    MaskEditText cnic;
    Spinner city, bloodgroup;

    CountryCodePicker ccp;
    EditText editTextCarrierNumber;
    LinearLayout box1, box2;

    RadioGroup radioSexGroup;
    RadioGroup radioTypeGroup;
    FirebaseAuth auth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);


        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);

        fname = findViewById(R.id.fname);
        lname = findViewById(R.id.lname);
        companyname = findViewById(R.id.companyname);
        email = findViewById(R.id.email);
        age = findViewById(R.id.age);
        address = findViewById(R.id.address);
        password = findViewById(R.id.password);
        cnic = findViewById(R.id.cnic);
        city = findViewById(R.id.citySignUp);
        genderText = findViewById(R.id.genderText);
        bloodgroup = findViewById(R.id.bloodgroup);
        ccp = (CountryCodePicker) findViewById(R.id.ccp);
        editTextCarrierNumber = (EditText) findViewById(R.id.editText_carrierNumber);
        ccp.registerCarrierNumberEditText(editTextCarrierNumber);


        box1 = findViewById(R.id.box1);
        box2 = findViewById(R.id.box2);
        box1.setVisibility(View.GONE);
        box2.setVisibility(View.VISIBLE);

        radioSexGroup = findViewById(R.id.radioSex);
        radioTypeGroup = findViewById(R.id.radioType);

        age.setFocusable(false);
        age.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                String date = String.format("%02d", dayOfMonth) + "-" + String.format("%02d", (monthOfYear + 1)) + "-" + year;
                                age.setText(date);
                            }
                        },
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.show(getSupportFragmentManager(), "Date Picker");

            }
        });

        auth = FirebaseAuth.getInstance();
        radioTypeGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {
                    case R.id.radioIndividual:
                        box1.setVisibility(View.GONE);
                        box2.setVisibility(View.VISIBLE);
                        cnic.setVisibility(View.VISIBLE);
                        genderText.setVisibility(View.VISIBLE);
                        radioSexGroup.setVisibility(View.VISIBLE);
                        break;
                    case R.id.radioCompany:
                        box1.setVisibility(View.VISIBLE);
                        box2.setVisibility(View.GONE);
                        radioSexGroup.setVisibility(View.GONE);
                        cnic.setVisibility(View.GONE);
                        genderText.setVisibility(View.GONE);
                        radioSexGroup.setVisibility(View.GONE);


                        break;
                }
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onSignup(View view) {

        final RadioButton radiotype = findViewById(radioTypeGroup.getCheckedRadioButtonId());

        if (radiotype.getTag().equals("individual")) {
            if (TextUtils.isEmpty(fname.getText().toString().trim()) || TextUtils.isEmpty(lname.getText().toString().trim())) {
                MyClass.showError(this, "Registeration Error!", "Please enter all required fields.");
                return;
            }
            if (TextUtils.isEmpty(age.getText().toString().trim()) || TextUtils.isEmpty(cnic.getRawText().trim()) || city.getSelectedItem().toString().equals("Select City") ||
                    bloodgroup.getSelectedItem().toString().equals("Select Blood Group") || TextUtils.isEmpty(password.getText().toString().trim())
                    || TextUtils.isEmpty(address.getText())) {
                MyClass.showError(this, "Registeration Error!", "Please enter all required fields.");
                return;
            }
            if (cnic.getRawText().length() < 13) {
                MyClass.showError(this, "Registeration Error!", "Please enter all required fields.");
                return;
            }
            if (!ccp.isValidFullNumber()) {
                MyClass.showError(this, "Inavlid phone number Error!", "Please enter a valid phone number.");
                return;
            }
//            if (editTextCarrierNumber.getText().toString().startsWith("0")) {
//                MyClass.showError(this, "Inavlid phone number Error!", "Please enter the rest of the phone number without 0 .");
//                return;
//            }
        } else {
            Log.d("SIGNUP", "onSignup: " + "here");
            if (TextUtils.isEmpty(companyname.getText().toString().trim())) {
                MyClass.showError(this, "Registeration Error!", "Please enter all required fields.");
                Toast.makeText(this, "here1", Toast.LENGTH_SHORT).show();

                return;
            }
            if (city.getSelectedItem().toString().equals("Select City") || TextUtils.isEmpty(password.getText().toString().trim())) {
                MyClass.showError(this, "Registeration Error!", "Please enter all required fields.");
                Toast.makeText(this, "here2", Toast.LENGTH_SHORT).show();

                return;
            }
            if (!ccp.isValidFullNumber()) {
                MyClass.showError(this, "Inavlid phone number Error!", "Please enter a valid phone number.");
                return;
            }
//            if (editTextCarrierNumber.getText().toString().startsWith("0")) {
//                MyClass.showError(this, "Inavlid phone number Error!", "Please enter the rest of the phone number without 0 .");
//                return;
//            }
        }


        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("registering user, please wait...");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        FirebaseDatabase.getInstance().getReference().child("users_updated").child(ccp.getFullNumberWithPlus())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        if (dataSnapshot.exists()) {
                            progressDialog.dismiss();
                            MyClass.showError(SignUpActivity.this, "Registeration Error!", "Mobile number already exist.");
                            return;
                        }
                        FirebaseDatabase.getInstance().getReference().child("users_updated")
                                .orderByChild("cnic").equalTo(cnic.getRawText())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()) {
                                            progressDialog.dismiss();
                                            MyClass.showError(SignUpActivity.this, "Registeration Error!", "Cnic number already exist");
                                            return;
                                        }
                                        auth.createUserWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                                                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                                        if (task.isSuccessful()) {
                                                            final RadioButton radiosex = findViewById(radioSexGroup.getCheckedRadioButtonId());
                                                            HashMap<String, Object> map = new HashMap<>();
                                                            if (radiotype.getTag().equals("individual")) {
                                                                map.put("firstname", fname.getText().toString());
                                                                map.put("lastname", lname.getText().toString());
                                                                map.put("email", email.getText().toString());
                                                                map.put("password", password.getText().toString());
                                                                map.put("country", ccp.getSelectedCountryEnglishName());
                                                                map.put("city", city.getSelectedItem().toString());
                                                                map.put("bloodgroup", bloodgroup.getSelectedItem().toString());
                                                                map.put("gender", radiosex.getTag().toString());
                                                                map.put("type", radiotype.getTag().toString());
                                                                map.put("age", age.getText().toString());
                                                                map.put("accountStatus", "enabled");
                                                                map.put("cnic", cnic.getRawText());
                                                                map.put("address", address.getText().toString());
                                                                map.put("uid", FirebaseAuth.getInstance().getCurrentUser().getUid());
                                                                String userDob = age.getText().toString();
                                                                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
                                                                LocalDate userDobFormatted = LocalDate.parse(userDob, dateTimeFormatter);
                                                                int userAge = Period.between(userDobFormatted, LocalDate.now()).getYears();
                                                                if (radiosex.getTag().toString().toLowerCase().equals("male")) {
                                                                    if (userAge >= 18 && userAge < 50) {
                                                                        map.put("donationStatus", true);
                                                                        map.put("phonePreference", true);
                                                                    } else if (userAge < 18) {
                                                                        map.put("donationStatus", false);
                                                                        map.put("phonePreference", false);
                                                                    } else {
                                                                        map.put("donationStatus", false);
                                                                        map.put("phonePreference", false);
                                                                    }
                                                                } else {
                                                                    if (userAge < 18) {
                                                                        map.put("donationStatus", false);
                                                                        map.put("phonePreference", false);
                                                                    } else {
                                                                        map.put("donationStatus", false);
                                                                        map.put("phonePreference", false);
                                                                    }
                                                                }

                                                                FirebaseDatabase.getInstance().getReference().child("users_updated").child(ccp.getFullNumberWithPlus())
                                                                        .setValue(map)
                                                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                            @Override
                                                                            public void onComplete(@NonNull Task<Void> task) {

                                                                                progressDialog.dismiss();

                                                                                if (!task.isSuccessful())
                                                                                    return;
                                                                                SharedPreferences sp = getSharedPreferences("LoginCredientials", 0);
                                                                                SharedPreferences.Editor Ed = sp.edit();
                                                                                Ed.putString("phone", ccp.getFullNumberWithPlus());
                                                                                Ed.putBoolean("phonePreference", false);
                                                                                Ed.putBoolean("donationStatus", false);
                                                                                Ed.putString("age", age.getText().toString());
                                                                                Ed.putString("gender", radiosex.getTag().toString());
                                                                                Ed.putString("type", radiotype.getTag().toString());

                                                                                Ed.apply();

                                                                                Intent intent = new Intent(SignUpActivity.this, ReceptorActivity.class);
                                                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                                startActivity(intent);
                                                                                finish();
                                                                            }
                                                                        });
                                                            } else {
                                                                map.put("companyname", companyname.getText().toString());
                                                                map.put("companyEmailAdress", email.getText().toString());
                                                                map.put("password", password.getText().toString());
                                                                map.put("country", ccp.getSelectedCountryEnglishName());
                                                                map.put("type", radiotype.getTag().toString());
                                                                map.put("city", city.getSelectedItem().toString());
                                                                map.put("bloodgroup", bloodgroup.getSelectedItem().toString());
                                                                FirebaseDatabase.getInstance().getReference().child("users_updated").child(ccp.getFullNumberWithPlus())
                                                                        .setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                    @Override
                                                                    public void onComplete(@NonNull Task<Void> task) {
                                                                        progressDialog.dismiss();
                                                                        if (!task.isSuccessful())
                                                                            return;
                                                                        SharedPreferences sp = getSharedPreferences("LoginCredientials", 0);
                                                                        SharedPreferences.Editor Ed = sp.edit();
                                                                        Ed.putString("phone", ccp.getFullNumberWithPlus());
                                                                        Ed.putString("type", radiotype.getTag().toString());

                                                                        Ed.apply();

                                                                        Intent intent = new Intent(SignUpActivity.this, ReceptorActivity.class);
                                                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                        startActivity(intent);
                                                                        finish();
                                                                    }
                                                                });

                                                            }


                                                        } else {
                                                            MyClass.showError(SignUpActivity.this, "Error", Objects.requireNonNull(task.getException()).toString());
                                                            progressDialog.dismiss();
                                                        }

                                                    }
                                                });
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError error) {

                                    }

                                });
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


    }


}
