package com.blood.savebleeding;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.santalu.maskedittext.MaskEditText;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ForgotPasswordActivity extends AppCompatActivity {

    EditText userEmail;
//    MaskEditText mobile;
    Button submit;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        userEmail = findViewById(R.id.user_email);
//        mobile = findViewById(R.id.forgot_password_mobile);
        submit = findViewById(R.id.btnSubmit);
        auth = FirebaseAuth.getInstance();
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(Objects.requireNonNull(userEmail.getText()).toString().trim())) {
                    MyClass.showError(ForgotPasswordActivity.this, "Input Error", "Please enter all required fields.");
                    return;
                }

                final ProgressDialog progressDialog = new ProgressDialog(ForgotPasswordActivity.this);
                progressDialog.setMessage("Sending a reset Password Email , please wait...");
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.show();

                auth.sendPasswordResetEmail(userEmail.getText().toString())
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                progressDialog.dismiss();
                                Toast.makeText(ForgotPasswordActivity.this, "Email Sent", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
                                startActivity(intent);
                                finish();

                            }
                        });

//                FirebaseDatabase.getInstance().getReference().child("users_updated").orderByKey().
//                        equalTo(mobile.getText().toString()).addListenerForSingleValueEvent(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                        if (dataSnapshot.exists()) {
//                            for (DataSnapshot dsp : dataSnapshot.getChildren()) {
//                                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("users_updated").child(Objects.requireNonNull(dsp.getKey()));
//                                Map<String, Object> updates = new HashMap<String, Object>();
//                                updates.put("password", newPassword.getText().toString());
//                                ref.updateChildren(updates);
//                                Toast.makeText(ForgotPasswordActivity.this, "Password Updated Successfully", Toast.LENGTH_SHORT).show();
//                                progressDialog.dismiss();
//                                startActivity(new Intent(ForgotPasswordActivity.this, LoginActivity.class));
//                            }
//                        } else {
//                            MyClass.showError(ForgotPasswordActivity.this, "No User Found ", "Make sure you type the phone number you used to create your account");
//                        }
//                    }
//
//                    @Override
//                    public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                    }
//                });

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}