package com.blood.savebleeding;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.threeten.bp.LocalDate;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.temporal.ChronoUnit;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

public class StatusHistoryActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private HistoryAdapter historyAdapter;
    private List<String> statusList = new ArrayList<>();
    private FloatingActionButton addStatusButton;
    LinearLayout no_item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_history);
        addStatusButton = findViewById(R.id.floating_action_button);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        no_item = findViewById(R.id.no_item);

        recyclerView = findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        historyAdapter = new HistoryAdapter(this, statusList);
        recyclerView.setAdapter(historyAdapter);
//        SharedPreferences sp1 = getSharedPreferences("LoginCredientials", 0);
//        int userAge = Integer.parseInt(Objects.requireNonNull(sp1.getString("age", "0")));
//        String userGender = sp1.getString("gender", "none");
//        if (userAge < 18) {
//            donationBox.setVisibility(View.GONE);
//        } else if (userGender.toLowerCase().equals("male") && userAge > 50) {
//            donationBox.setVisibility(View.VISIBLE);
//            checkUserData();
//        } else if (userGender.toLowerCase().equals("female") && userAge > 18) {
//            donationBox.setVisibility(View.VISIBLE);
//            checkUserData();
//        } else if (userGender.toLowerCase().equals("male") && userGender.toLowerCase().equals("male") && userAge <= 50) {
//            donationBox.setVisibility(View.GONE);
//        } else {
//            checkUserData();
//
//        }

        initFirebase();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void initFirebase() {
        FirebaseDatabase.getInstance().getReference().child("history").child(MyClass.user.getMobile())
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                        String status = dataSnapshot.getValue(String.class);

                        statusList.add(status);
                        historyAdapter.notifyDataSetChanged();

                        if (no_item.getVisibility() == View.VISIBLE)
                            no_item.setVisibility(View.GONE);
                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    public void onStatus(View view) {
            FirebaseDatabase.getInstance().getReference().child("history").
                    child(MyClass.user.getMobile()).limitToLast(1).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists())
                    {
                        for (DataSnapshot dsp : dataSnapshot.getChildren()) {
                            String Date = dsp.getValue(String.class);
                            LocalDate currentDate = LocalDate.now();
                            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
                            assert Date != null;
                            LocalDate donationDate = LocalDate.parse(Date, dateTimeFormatter);
                            long daysDuaration = ChronoUnit.DAYS.between(donationDate, currentDate);
                            if (daysDuaration > 90) {
                                setStatusDate();
                            } else {
                                MyClass.showError(StatusHistoryActivity.this, "Days Limit", "You can add event after 90 days" +
                                        ".Your current passed days after last donation are " + daysDuaration);
                            }
                        }
                    }
                    else
                    {
                        setStatusDate();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

    }

    private void setStatusDate() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        String date = String.format("%02d", dayOfMonth) + "-" + String.format("%02d", (monthOfYear + 1)) + "-" + year;
                        final ProgressDialog progressDialog = new ProgressDialog(StatusHistoryActivity.this);
                        progressDialog.setMessage("submitting, please wait...");
                        progressDialog.setCancelable(false);
                        progressDialog.setCanceledOnTouchOutside(false);
                        progressDialog.show();

                        FirebaseDatabase.getInstance().getReference().child("history").child(MyClass.user.getMobile())
                                .push().setValue(date)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        progressDialog.dismiss();

                                        Toast.makeText(StatusHistoryActivity.this, "Status Added Successfully", Toast.LENGTH_LONG).show();
                                    }
                                });
                    }
                },
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getSupportFragmentManager(), "Date Picker");
        dpd.setVersion(DatePickerDialog.Version.VERSION_2);
    }
}
