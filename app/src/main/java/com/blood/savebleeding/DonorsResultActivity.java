package com.blood.savebleeding;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class DonorsResultActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private DonorsResultAdapter donorsResultAdapter;
    private List<User> userList = new ArrayList<>();

    LinearLayout no_item;

    String country, city, bloodgroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donors_result);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        country = getIntent().getStringExtra("country");
        city = getIntent().getStringExtra("city");
        bloodgroup = getIntent().getStringExtra("bloodgroup");

        no_item = findViewById(R.id.no_item);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        donorsResultAdapter = new DonorsResultAdapter(this, userList);
        recyclerView.setAdapter(donorsResultAdapter);

        initFirebase();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void initFirebase() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Finding match, please wait...");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        FirebaseDatabase.getInstance().getReference().child("users_updated").orderByChild("city")
                .equalTo(city)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        if (dataSnapshot.exists()) {
                            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                User user = snapshot.getValue(User.class);
                                assert user != null;
                                user.setMobile(snapshot.getKey());

                                if (user.getCountry().equals(country) && user.getBloodgroup().equals(bloodgroup)
                                        && user.getType().equals("individual")
                                        && !MyClass.user.getMobile().equals(snapshot.getKey())
                                        && !user.getAccountStatus().equals("disabled")
                                        && MyClass.getAgeByDob(user.getAge()) >= 18) {
                                    userList.add(user);
                                    donorsResultAdapter.notifyDataSetChanged();

                                    if (no_item.getVisibility() == View.VISIBLE)
                                        no_item.setVisibility(View.GONE);
                                }

                                progressDialog.dismiss();
                            }
                        } else
                            progressDialog.dismiss();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        progressDialog.dismiss();
                    }
                });
    }


}
