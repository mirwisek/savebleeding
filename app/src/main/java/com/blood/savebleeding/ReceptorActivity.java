package com.blood.savebleeding;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.util.Log;
import android.view.View;

import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import de.hdodenhof.circleimageview.CircleImageView;

import android.view.MenuItem;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.threeten.bp.LocalDate;
import org.threeten.bp.Period;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.temporal.ChronoUnit;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ReceptorActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    TextView navUsername;
    CircleImageView profileimg;
    AutoCompleteTextView searchView;
    Button statusHistrotyButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receptor);
        searchView = findViewById(R.id.searchView);
        statusHistrotyButton = findViewById(R.id.statusDonorButton);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);
        navUsername = headerView.findViewById(R.id.name);
        profileimg = headerView.findViewById(R.id.image);
        getUser();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateUserData();
        SharedPreferences sp1 = getSharedPreferences("LoginCredientials", 0);
        String phone = sp1.getString("phone", "none");
        String type = sp1.getString("type","individual");
        if (type.equals("individual")) {
            FirebaseDatabase.getInstance().getReference().child("users_updated").child(phone)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            MyClass.user = snapshot.getValue(User.class);
                            MyClass.user.setMobile(snapshot.getKey());

                            Log.d("PhoneNumber", "onDataChange: " + MyClass.user.getMobile());

                            checkUserAge();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.action_profile) {
            startActivity(new Intent(this, ProfileActivity.class));
        } else if (id == R.id.action_logout) {
            logout();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

//    public void onDonationStatusClicked(View view) {
//        SharedPreferences sp1 = getSharedPreferences("LoginCredientials", 0);
//        String phone = sp1.getString("phone", "none");
//        int userAge = Integer.parseInt(Objects.requireNonNull(sp1.getString("age", "0")));
//        String userGender = sp1.getString("gender", "none");
//        if (donationStatus.isChecked()) {
//            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("users_updated").child(phone);
//            Map<String, Object> updates = new HashMap<String, Object>();
//            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    switch (which) {
//                        case DialogInterface.BUTTON_POSITIVE:
//                            updates.put("phonePreference", true);
//                            updates.put("donationStatus", true);
//                            ref.updateChildren(updates);
//                            donationStatus.setChecked(true);
//                            break;
//
//                        case DialogInterface.BUTTON_NEGATIVE:
//                            updates.put("donationStatus", false);
//                            updates.put("phonePreference", false);
//                            ref.updateChildren(updates);
//                            donationStatus.setChecked(false);
//
//                            break;
//                    }
//                }
//            };
//            MyClass.ShowDonationStatusDialogBox(this, dialogClickListener, "Attention", "If you will turn Donation Status " +
//                    "on then your phone number will be shown when people will search for users." +
//                    "Do yo want to continue");
//
//        } else {
//            assert phone != null;
//            MyClass.showError(this, "Attention", "If Donation Status is " +
//                    "OFF then your phone number will not be shown when people will search for users.");
//            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("users_updated").child(phone);
//            Map<String, Object> updates = new HashMap<String, Object>();
//            updates.put("donationStatus", false);
//            updates.put("phonePreference", false);
//            ref.updateChildren(updates);
//            donationStatus.setChecked(false);
//
//        }
//    }

//    public void onAccountStatusClicked(View view) {
//
//        SharedPreferences sp1 = getSharedPreferences("LoginCredientials", 0);
//        String phone = sp1.getString("phone", "none");
//        if (accountStatus.isChecked()) {
//            assert phone != null;
//            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("users_updated").child(phone);
//            Map<String, Object> updates = new HashMap<String, Object>();
//            updates.put("accountStatus", "disabled");
//            ref.updateChildren(updates);
//        } else {
//            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("users_updated").child(phone);
//            Map<String, Object> updates = new HashMap<String, Object>();
//            updates.put("accountStatus", "enabled");
//            ref.updateChildren(updates);
//        }
//
//    }

    public void onFind(View view) {
        startActivity(new Intent(this, FindDonorsActivity.class));
    }

    public void onStatusHistory(View view) {
        startActivity(new Intent(this, StatusHistoryActivity.class));
    }

    public void onFindByCnic(View view) {
        startActivity(new Intent(this, FindDonorsByCnicActivity.class));
    }


    void getUser() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Initializing, please wait...");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        SharedPreferences sp1 = getSharedPreferences("LoginCredientials", 0);
        String phone = sp1.getString("phone", "none");
        String type = sp1.getString("type","individual");

        FirebaseDatabase.getInstance().getReference().child("users_updated").child(phone)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        progressDialog.dismiss();

                        if (dataSnapshot.exists()) {
                            MyClass.user = dataSnapshot.getValue(User.class);
                            assert type != null;
                            if (type.equals("individual")) {
                                checkUserAge();
                            }
                            MyClass.user.setMobile(dataSnapshot.getKey());
                            Log.d("PhoneNumber", "onDataChange: " + MyClass.user.getMobile());


                            if (MyClass.user.getType().equals("individual")) {
                                navUsername.setText(MyClass.user.getFullname());
                            } else {
                                navUsername.setText(MyClass.user.getCompanyname());
                            }

                            Picasso.get()
                                    .load(MyClass.user.getImage())
                                    .placeholder(R.drawable.image_default)
                                    .into(profileimg);
                        } else {
                            logout();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    void updateUserData() {
        if (MyClass.user == null)
            return;

        if (MyClass.user.getType().equals("individual")) {
            navUsername.setText(MyClass.user.getFullname());
        } else {
            navUsername.setText(MyClass.user.getCompanyname());
        }

        Picasso.get()
                .load(MyClass.user.getImage())
                .placeholder(R.drawable.image_default)
                .into(profileimg);
    }

    private void checkUserAge() {
        SharedPreferences sp1 = getSharedPreferences("LoginCredientials", 0);
        String userDob = MyClass.user.age;
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate userDobFormatted = LocalDate.parse(userDob, dateTimeFormatter);
        int userAge = Period.between(userDobFormatted, LocalDate.now()).getYears();
        String userGender = MyClass.user.gender;
        Log.d("userCredentials", "checkUserAge: " + userGender + userAge);
        if (MyClass.user.getGender().toLowerCase().equals("male")) {
            if (userAge >= 18 && userAge < 50) {
                statusHistrotyButton.setVisibility(View.VISIBLE);
                if (MyClass.user.getAccountStatus().equals("disabled")) {
                    statusHistrotyButton.setVisibility(View.GONE);
                } else {
                    statusHistrotyButton.setVisibility(View.VISIBLE);
                }
            } else if (userAge < 18) {
                Log.d("GHE", "checkUserAge: " + "Here");
                statusHistrotyButton.setVisibility(View.GONE);
            }
            else {
                statusHistrotyButton.setVisibility(View.VISIBLE);
                if (MyClass.user.getAccountStatus().equals("disabled")) {
                    statusHistrotyButton.setVisibility(View.GONE);
                } else {
                    statusHistrotyButton.setVisibility(View.VISIBLE);

                }
            }
        } else {
            if (userAge < 18) {
                statusHistrotyButton.setVisibility(View.GONE);
            } else {
                statusHistrotyButton.setVisibility(View.VISIBLE);
            }
            if (MyClass.user.getAccountStatus().equals("disabled")) {
                statusHistrotyButton.setVisibility(View.GONE);
            } else {
                statusHistrotyButton.setVisibility(View.VISIBLE);

            }
        }

//
//        if (userAge < 18) {
//            statusHistrotyButton.setVisibility(View.GONE);
//        } else {
//            if (userGender.toLowerCase().equals("male") && userAge > 50) {
//                statusHistrotyButton.setVisibility(View.VISIBLE);
//            } else if (userGender.toLowerCase().equals("male") && userAge <= 50) {
//                statusHistrotyButton.setVisibility(View.GONE);
//            } else if (userGender.toLowerCase().equals("male") && userAge == 18) {
//                statusHistrotyButton.setVisibility(View.VISIBLE);
//            } else if (userGender.toLowerCase().equals("female")) {
//                statusHistrotyButton.setVisibility(View.VISIBLE);
//            }
//        }


    }

//    private void checkUserHistoryData() {
//        SharedPreferences sp1 = getSharedPreferences("LoginCredientials", 0);
//        boolean donationvalue = sp1.getBoolean("donationStatus", true);
//        String phone = sp1.getString("phone", "none");
//        //if user has some existing data
//        FirebaseDatabase.getInstance().getReference().child("history").
//                child(phone).limitToLast(1).addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                if (dataSnapshot.exists()) {
//                    for (DataSnapshot dsp : dataSnapshot.getChildren()) {
//                        String Date = dsp.getValue(String.class);
//                        LocalDate currentDate = LocalDate.now();
//                        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
//                        assert Date != null;
//                        LocalDate donationDate = LocalDate.parse(Date, dateTimeFormatter);
//                        long daysDuaration = ChronoUnit.DAYS.between(donationDate, currentDate);
//                        if (daysDuaration <= 90) {
////                            donationStatus.setClickable(false);
//                            donationStatus.setChecked(false);
//                            donationStatus.setEnabled(false);
//                            Toast.makeText(ReceptorActivity.this, "You can't turn on Donation Status because the 90 day limit is not over yet", Toast.LENGTH_SHORT).show();
//
//                        } else {
//                            donationStatus.setEnabled(true);
//                        }
//
//                    }
//                }
////                else {
////
////                    if (donationvalue) {
////                        donationStatus.setChecked(true);
////                    } else {
////                        donationStatus.setChecked(false);
////                    }
////                }
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
//    }

    void logout() {
        SharedPreferences sp = getSharedPreferences("LoginCredientials", 0);
        SharedPreferences.Editor Ed = sp.edit();
        Ed.putString("phone", "none");
        Ed.commit();
        FirebaseAuth.getInstance().signOut();
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }


}
