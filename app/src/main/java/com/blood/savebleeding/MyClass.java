package com.blood.savebleeding;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.threeten.bp.LocalDate;
import org.threeten.bp.Period;
import org.threeten.bp.format.DateTimeFormatter;

import java.util.HashMap;
import java.util.Map;

public class MyClass {

    public static final int RC_CALL = 1;

    public static User user = null;

    public static void showError(Context context, String title, String error)
    {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(error)
                .setPositiveButton("ok", null)
                .show();
    }

    public static void  ShowDonationStatusDialogBox(Context context ,DialogInterface.OnClickListener dialogClickListener , String title , String msg)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title).setMessage(msg).setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }
    public static int getAgeByDob(String dob)
    {
        String userDob = dob;
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate userDobFormatted = LocalDate.parse(userDob, dateTimeFormatter);
        int userAge = Period.between(userDobFormatted, LocalDate.now()).getYears();
        return userAge;
    }

}
